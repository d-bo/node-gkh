'use strict';

const fs = require('fs');
const mysql = require('mysql2/promise');
const colors = require('colors');
const _ = require('lodash');
const utils = require('./lib/utils');
const house = require('./lib/libhouse');
const mongo = require('./lib/libmongo');
const log4js = require('log4js');
const dateFormat = require('dateformat');
const date = require('date-and-time');
const addr = require('./lib/libaddr');
const term = require('terminal-kit').terminal;

const {chain}  = require('stream-chain');
const {parser} = require('stream-json');
const {pick}   = require('stream-json/filters/Pick');
const {streamArray} = require('stream-json/streamers/StreamArray');
const AlignStream = require('utf8-align-stream');


/**
  Truncate before count `t4qz8_count_org`
  cron: node house.js truncate
*/
const TruncateCount = async () => {
  var connection = await mysql.createConnection({
    host     : process.env.GKH_MYSQL_HOST,
    user     : process.env.GKH_MYSQL_USER,
    password : process.env.GKH_MYSQL_PASSWORD,
    database : process.env.GKH_MYSQL_DATABASE
  });
  var [rows, fields] = await connection.query(`TRUNCATE TABLE t4qz8_count_org;`);
  console.log("TRUNCATED `t4qz8_count_org`".green);
  connection.end();
}


/**
  Update `t4qz8_house_organizations` from `t4qz8_count_org`
  cron: node house.js org_update_count
*/
const OrgUpdateCount = async () => {

  var connection = await mysql.createConnection({
    host     : process.env.GKH_MYSQL_HOST,
    user     : process.env.GKH_MYSQL_USER,
    password : process.env.GKH_MYSQL_PASSWORD,
    database : process.env.GKH_MYSQL_DATABASE
  });

  var [rows, fields] = await connection.query(
    `UPDATE t4qz8_organization org LEFT JOIN t4qz8_count_org orgC 
    ON org.id = orgC.org_id SET houses = orgC.house_total, 
    fond = orgC.square_total`
    );

  console.log("UPDATE ORGS".green, "t4qz8_count_org".yellow, rows);

  connection.end();
}


/**
  Update `t4qz8_house_organizations` from `t4qz8_count_org`
  cron: node house.js org_update_count
*/
const OrgUpdateSchools = async () => {

  var connection = await mysql.createConnection({
    host     : process.env.GKH_MYSQL_HOST,
    user     : process.env.GKH_MYSQL_USER,
    password : process.env.GKH_MYSQL_PASSWORD,
    database : process.env.GKH_MYSQL_DATABASE
  });

  var [rows, fields] = await connection.query(
    `UPDATE t4qz8_organization_house org_h
    LEFT JOIN t4qz8_organization org on org_h.organization_id=org.id
    SET org_h.published=1 WHERE org.type_id>1;`
    );

  console.log("UPDATE SCHOOLS".green, "t4qz8_organization_house".yellow, rows);

  connection.end();
}


// CLI params ?
var ForceRegionParse = null;
if (process.argv[2] != 'undefined') {
  ForceRegionParse = process.argv[2];
  if (ForceRegionParse == 'truncate') {
    TruncateCount();
    return;
  }
  if (ForceRegionParse == 'org_update_count') {
    OrgUpdateCount();
    return;
  }
  if (ForceRegionParse == 'org_update_schools') {
    OrgUpdateSchools();
    return;
  }
}


/**
  Insert house
  @param orgObj {object} Organization object
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
const CheckHouseAwait = async (orgObj, conn, logger) => {

  var connection = conn;

  var cityTypes = {};
  var areaTypes = {};
  var streetTypes = {};
  var streetNotFound = {};

  var parsed, houseNumber;

  var search = {};
  var area, areaType;

  var MISSING_STREET = false;

  var search = addr.FormatSearch(orgObj);

  if (!search) {
    return;
  }

  term.saveCursor();
  term.eraseLine();
  term(orgObj['guid']);
  term.restoreCursor();

  if (!search) {
    return;
  }

  if (search) {

    // Get or insert new `t4qz8_settlement.sub_region_id`
    if (search.area !== undefined) {
      search.sub_region_id = await addr.GetSubRegion(search, connection, logger);
    } else {
      search.sub_region_id = undefined;
    }

    // Get or insert `t4qz8_settlement`
    search.settlement_id = await addr.GetSettlement(search, connection, logger);
    if (search.settlement_id === undefined) return;

    // Get or insert `t4qz8_street`
    search.street_id = await addr.GetStreet(search, connection, logger);
    if (search.street_id === undefined) return;

    // House insert or update
    search.house_id = await house.Insert(search, connection, logger);
    if (search.house_id === undefined) return;

    // Insert house description
    await house.InsertDesc(search, connection, logger, orgObj);

    // Management organization
    if (orgObj.hasOwnProperty('managementOrganization')) {

      var [rows8, fields8] = await connection.query(
        `SELECT id FROM t4qz8_organization 
        WHERE guid='${orgObj['managementOrganization']['guid']}';`);
      if (rows8.length < 1) {
        // cannot insert - low info
      } else {
        search.org_id = rows8[0].id;

        // reset org house links, published=0
        try {
          var [rows112, fields112] = await connection.query(
            `UPDATE t4qz8_organization_house SET published=0 
            WHERE house_id='${search.house_id}';`
          );
        } catch(e) {
          //pass
        }

        // Management organization contract date
        let managementContractDate = 'NULL';
        if (orgObj.hasOwnProperty('managementContractDate')) {
          managementContractDate = orgObj['managementContractDate'].replace(/\./g, '-');
          managementContractDate = dateFormat(date.parse(managementContractDate, 
            'DD-MM-YYYY'), "yyyy-mm-dd");
          managementContractDate = `"${managementContractDate}"`;
        }
        try {
          var [rows11, fields11] = await connection.query(
            `INSERT INTO t4qz8_organization_house (organization_id, house_id, published, 
            contract_date) VALUES ('${search.org_id}', '${search.house_id}', 1, 
            ${managementContractDate}) ON DUPLICATE KEY UPDATE published=1, 
            contract_date=${managementContractDate};`
          );
        } catch(e) {
          //pass
        }

        // Update counters
        if (orgObj.hasOwnProperty('totalSquare')) {

          var summ = orgObj['totalSquare'];

          try {
            var [rows11, fields11] = await connection.query(
              `INSERT INTO t4qz8_count_org (org_id, square_total, house_total) 
              VALUES ('${search.org_id}', '${summ}', 1) ON DUPLICATE KEY 
              UPDATE square_total = square_total + ${summ}, 
              house_total = house_total + 1;`
            );
          } catch(e) {
            console.log("INSERT INTO t4qz8_count_org".red, search, e);
            logger.debug("INSERT INTO t4qz8_count_org", search, e);
          }
        }

        term.saveCursor();
        term.eraseLine();
        term(search.guid, "HOUSE_ID".green, search.house_id, "ORG".yellow, search.org_id);
        term.restoreCursor();
      }

    } else {
      term.saveCursor();
      term.eraseLine();
      term(search.guid, "HOUSE_ID".green, search.house_id, "NO ORG".magenta);
      term.restoreCursor();
    }

  }

}



// Init
async function main() {

  // Mongo installed ?
  // Ready to insert
  var MONGODB_ACTIVE = true;

  try {
    var collection = await mongo.Connect('gkh','houses');
  } catch(e) {
    MONGODB_ACTIVE = false;
  }

  // Mysql
  const connection = await mysql.createConnection({
    host     : process.env.GKH_MYSQL_HOST,
    user     : process.env.GKH_MYSQL_USER,
    password : process.env.GKH_MYSQL_PASSWORD,
    database : process.env.GKH_MYSQL_DATABASE
  });

  if (ForceRegionParse) {

    console.log("\nPARSING HOUSES".green, ForceRegionParse);

    log4js.configure({
      appenders: { house: { type: 'file', filename: 'house_'+ForceRegionParse+'.log' } },
      categories: { default: { appenders: ['house'], level: 'debug' } }
    });

    var logger = log4js.getLogger('house');

    var pipeline = chain([
      fs.createReadStream('./json/extracted/'+ForceRegionParse+'/houses.json'),
      new AlignStream(),
      parser(),
      pick({filter: 'houses'}),
      streamArray(),
      async data => {
        if (MONGODB_ACTIVE) {
          await mongo.DeleteThenInsertObj(data.value);
        }
        await CheckHouseAwait(data.value, connection, logger);
      }
    ]);

    let counter = 0;
    pipeline.on('data', () => ++counter);
    pipeline.on('end', async () => {
      if (MONGODB_ACTIVE) await mongo.Close();
      await connection.end();
      term.saveCursor();
      term.eraseLine();
      term("FINISHED HOUSE ".magenta, ForceRegionParse);
      term.restoreCursor();
    });

  } else {
    console.log("ERR: NO REGION PROVIDED".red);
    //logger.debug("ERR: NO REGION PROVIDED");
  }
  return;
}

main();