'use strict';

/**
  Download .json files from octonica ftp folder
*/

const fs = require('fs');
const _ = require('lodash');
const unzip = require('unzip');
const mkdirp = require('mkdirp');
const colors = require('colors');
const ftpClient = require('ftp-client');

var config = {
    host: process.env.OCTONICA_FTP_HOST,
    port: 21,
    user: process.env.OCTONICA_FTP_USER,
    password: process.env.OCTONICA_FTP_PASSWORD
}

var client = new ftpClient(config, {'logging': 'debug', 'overwrite': 'all'});

client.connect(function() {
	client.download('', './json/zip', {'overwrite': 'all'}, function(d, err) {
    console.log("DD".magenta, d);
    if (err != undefined) {
      console.log(err);
    } else {
      console.log("RESULT".green, d);
      for (var file in d.downloadedFiles) {
        var path = './json/zip/';
        var zippath = d.downloadedFiles[file].replace(/\//g, '');
        var zipdir = zippath.replace(/\.zip/g, '');
        console.log("FILE", path+zippath);

        fs.createReadStream(path+zippath)
        .pipe(unzip.Extract({ path: './json/extracted/'+zipdir }))
        .on('close', function() {
          console.log("DONE ZIP".green);
        });

      }
    }
  });
});