
/**
  Tests
*/

const mysql = require('mysql2/promise');
const should = require('chai').should;
const expect = require('chai').expect;

describe('#undefined', async () => {

  let connection;

  before(async () => {
    connection = await mysql.createConnection({
      host     : process.env.GKH_MYSQL_HOST,
      user     : process.env.GKH_MYSQL_USER,
      password : process.env.GKH_MYSQL_PASSWORD,
      database : process.env.GKH_MYSQL_DATABASE
    });
  });

  after(async () => {
    connection.end();
  });

  it('check table t4qz8_settlement', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_settlement where name like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_street', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_street where name like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_sub_region', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_sub_region where name like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_house_description', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_house_description where material like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_organization', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_organization where name like '%undefined%' or short_name like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_organization_type', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_organization_type where name like '%undefined%' or short_name like '%undefined%';`
      );
    expect(rows).to.be.empty;
  });

});

describe('#damaged records with ��', async () => {

  let connection;

  before(async () => {
    connection = await mysql.createConnection({
      host     : process.env.GKH_MYSQL_HOST,
      user     : process.env.GKH_MYSQL_USER,
      password : process.env.GKH_MYSQL_PASSWORD,
      database : process.env.GKH_MYSQL_DATABASE
    });
  });

  after(async () => {
    connection.end();
  });

  it('check table t4qz8_settlement', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_settlement where name like '%��%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_street', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_street where name like '%��%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_sub_region', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_sub_region where name like '%��%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_house_description', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_house_description where material like '%��%';`
      );
    expect(rows).to.be.empty;
  });

  it('check table t4qz8_organization', async () => {
    var [rows, fields] = await connection.query(
      `select id from t4qz8_organization where name like '%��%' or short_name like '%��%';`
      );
    expect(rows).to.be.empty;
  });

});

describe('#double settlements, houses, streets ...', async () => {

  let connection;

  before(async () => {
    connection = await mysql.createConnection({
      host     : process.env.GKH_MYSQL_HOST,
      user     : process.env.GKH_MYSQL_USER,
      password : process.env.GKH_MYSQL_PASSWORD,
      database : process.env.GKH_MYSQL_DATABASE
    });
  });

  after(async () => {
    connection.end();
  });

  it('check Мытищи', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_settlement where name='Мытищи';`
      );
    expect(rows).to.have.lengthOf(1);
  });

  it('check Дмитров', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_settlement where name='Дмитров';`
      );
    expect(rows).to.have.lengthOf(1);
  });

  it('check Волоколамск', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_settlement where name='Волоколамск';`
      );
    expect(rows).to.have.lengthOf(1);
  });

  it('check Шихово', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_settlement where name='Шихово';`
      );
    expect(rows).to.have.lengthOf(1);
  });

  it('заведена ТСН "ЖК СОЧИ"', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_organization where short_name='ТСН "ЖК СОЧИ"';`
      );
    if (rows.length > 0) {
      const sochi_org = rows[0].id;
    }
    expect(rows).to.have.lengthOf(1);
  });

  it('должна быть привязка ЖК СОЧИ (Екатеринбург, Авиационная, 10)', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_organization where short_name='ТСН "ЖК СОЧИ"';`
      );
    if (rows.length > 0) {
      var [rows1, fields1] = await connection.query(
        `SELECT * FROM t4qz8_organization_house where organization_id=${rows[0].id};`
        );
      expect(rows1).to.have.lengthOf(1);
    }
  });

  it('Один дом по адресу г.Видное, ул. Ольховая, 9', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_settlement where name='Видное';`
      );
    expect(rows).to.have.lengthOf(1);
    if (rows.length > 0) {
      var [rows1, fields1] = await connection.query(
        `SELECT * FROM t4qz8_street where settlement_id=${rows[0].id} and name='Ольховая';`
        );
      expect(rows1).to.have.lengthOf(1);
      if (rows1.length > 0) {
        var [rows2, fields1] = await connection.query(
          `SELECT * FROM t4qz8_house where settlement_id=${rows[0].id} 
          and street_id=${rows1[0].id} and house=9;`
          );
        expect(rows2).to.have.lengthOf(1);
      }
    }
  });

  it('material field is used in `t4qz8_house_description` ', async () => {
    var [rows, fields] = await connection.query(
      `SELECT id FROM t4qz8_house_description where material != '';`
      );
    expect(rows).to.not.have.lengthOf(1);
  });

  it('contract_date field is used in `t4qz8_organization_house` ', async () => {
    var [rows, fields] = await connection.query(
      `SELECT * FROM t4qz8_organization_house WHERE contract_date IS NOT NULL;`
      );
    expect(rows).to.not.have.lengthOf(1);
  });

});
