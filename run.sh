#!/bin/bash

node ftp.js

mkdir bkp
mv *.log bkp/

for d in json/extracted/* ; do
    echo "$d"|node org.js `cut -f3  -d'/'`
done

node house.js truncate

for d in json/extracted/* ; do
    echo "$d"|node house.js `cut -f3  -d'/'`
done

node house.js org_update_count
node house.js org_update_schools
