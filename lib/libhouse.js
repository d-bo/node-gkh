'use strict';


/**
  Insert or update house
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
exports.Insert = async (search, conn, logger) => {

  var connection = conn;

  // House step #1
  // Try to search house formatted by '/' - '15/12А'
  // if exist - set search.house_id
  if (search.buildingNumber !== undefined && search.houseNumber !== undefined) {
    var queryHouse = search.houseNumber + '/' + search.buildingNumber;
    if (search.structNumber !== undefined) {
      queryHouse += 'с' + search.structNumber;    // ex. 15/12Ас15
    }

    if (search.houseGuid !== undefined) {
      var [rows61, fields61] = await connection.query(
        `SELECT id FROM t4qz8_house WHERE (street_id=${search.street_id} 
        AND settlement_id='${search.settlement_id}' 
        AND region_id='${search.regionNumber}' AND house='${queryHouse}') 
        or guid='${search.houseGuid}';`);
    } else {
      var [rows61, fields61] = await connection.query(
        `SELECT id FROM t4qz8_house WHERE (street_id=${search.street_id} 
        AND settlement_id='${search.settlement_id}' 
        AND region_id='${search.regionNumber}' AND house='${queryHouse}');`);
    }

    if (rows61.length > 0) {
      search.house_id = rows61[0].id;

      // Update house guid
      if (search.houseGuid !== undefined) {
        try {
          var [rows611, fields611] = await connection.query(
            `UPDATE t4qz8_house SET guid='${search.houseGuid}' 
            WHERE id='${search.house_id}';`);
        } catch(e) {
          //pass
        }
      }
    }
  }

  // House step #2
  // Not found house formatted by '/'
  // Try to find formatted by 'к' - '15к12А'
  if (search.house_id === undefined) {

    var finalHouseNumber = '';

    // Format final house number
    if (search.houseNumber !== undefined) {
      finalHouseNumber += search.houseNumber;
    }
    if (search.buildingNumber !== undefined) {
      finalHouseNumber += 'к' + search.buildingNumber;
    }
    if (search.structNumber !== undefined) {
      finalHouseNumber += 'с' + search.structNumber;
    }

    search.houseNumber = finalHouseNumber;

    if (search.houseGuid !== undefined) {
      var [rows6, fields6] = await connection.query(
        `SELECT id FROM t4qz8_house WHERE (street_id=${search.street_id} 
        AND settlement_id='${search.settlement_id}' 
        AND region_id='${search.regionNumber}' 
        AND house='${search.houseNumber}') or guid='${search.houseGuid}';`);
    } else {
      var [rows6, fields6] = await connection.query(
        `SELECT id FROM t4qz8_house WHERE (street_id=${search.street_id} 
        AND settlement_id='${search.settlement_id}' 
        AND region_id='${search.regionNumber}' AND house='${search.houseNumber}');`);
    }

    if (rows6.length < 1) {
      try {
        if (search.houseGuid !== undefined) {
          var [rows7, fields7] = await connection.query(
            `INSERT INTO t4qz8_house (street_id, settlement_id, region_id, house, 
            real_region, guid, corps, type_id, hits) VALUES (${search.street_id}, 
            '${search.settlement_id}', '${search.regionNumber}', 
            '${search.houseNumber}', '${search.real_region}', '${search.houseGuid}', 
            '', 1, 0) ON DUPLICATE KEY UPDATE street_id=${search.street_id}, 
            settlement_id='${search.settlement_id}', region_id='${search.regionNumber}', 
            house='${search.houseNumber}', real_region='${search.real_region}', 
            guid='${search.houseGuid}';`);
        } else {
          var [rows7, fields7] = await connection.query(
            `INSERT INTO t4qz8_house (street_id, settlement_id, region_id, house, 
            real_region, corps, type_id, hits) VALUES (${search.street_id}, 
            '${search.settlement_id}', '${search.regionNumber}', 
            '${search.houseNumber}', '${search.real_region}', '', 1, 0) 
            ON DUPLICATE KEY UPDATE street_id=${search.street_id}, 
            settlement_id='${search.settlement_id}', region_id='${search.regionNumber}', 
            house='${search.houseNumber}', real_region='${search.real_region}';`);
        }

        search.house_id = rows7.insertId;

      } catch(e) {

        // will be no house_id
        // process next org
        return undefined;
      }

    } else {
      search.house_id = rows6[0].id;

      // Update house guid
      if (search.houseGuid !== undefined) {
        try {
          var [rows611, fields611] = await connection.query(
            `UPDATE t4qz8_house SET guid='${search.houseGuid}' 
            WHERE id='${search.house_id}';`);
        } catch(e) {
          //pass
        }
      }
    }
  }

  return search.house_id;
}


/**
  Insert house description (t4q8_house_description)
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
  @param orgObj {object} Organization object
*/
exports.InsertDesc = async (search, conn, logger, orgObj) => {

  var connection = conn;

  // Collect floor_count, square, building_year
  var floor_count = 0;
  var square = 0.00;
  var building_year = 0;

  if (orgObj.hasOwnProperty('maxFloorCount')) {
    floor_count = parseInt(orgObj['maxFloorCount']);
    if (!floor_count) {
      floor_count = 0;
    }
  }
  if (orgObj.hasOwnProperty('totalSquare')) {
    square = orgObj['totalSquare'];
    if (square > 10000000000) {
      square = 0;
    }
  }
  if (orgObj.hasOwnProperty('buildingYear')) {
    building_year = parseInt(orgObj['buildingYear']);
    if (!building_year) {
      building_year = 0;
    }
  }

  var material = '';
  if (orgObj.hasOwnProperty('intWallMaterialList')) {
    material = orgObj['intWallMaterialList'];
  }

  // INSERT UPDATE house_description
  try {
    var q = `INSERT INTO t4qz8_house_description (house_id, floor_count, square, 
    building_year, hits, material) VALUES ('${search.house_id}', '${floor_count}', 
    '${square}', '${building_year}', 0, '${material}') ON DUPLICATE KEY 
    UPDATE floor_count='${floor_count}', square='${square}', 
    building_year='${building_year}';`;
    var [rows61, rows] = await connection.query(q);
  } catch(e) {
    //pass
  }

}