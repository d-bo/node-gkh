'use strict';

const utils = require('./utils');
const term = require('terminal-kit').terminal;

/**
  Insert or update house
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
  @param orgObj {object} Organization object
*/
exports.Insert = async (search, conn, logger, orgObj) => {

  var connection = conn;

  if (orgObj.hasOwnProperty('AdditionalData')) {

    // Work time extract
    if (orgObj['AdditionalData'].hasOwnProperty('organizationOpeningHours')) {
      search.work_time = utils.ExtractWorkTime(
        orgObj['AdditionalData']['organizationOpeningHours']
        );
    }
    if (search.work_time === undefined) {
      search.work_time = '';
    }

    var re = /'/g;

    if (orgObj['AdditionalData'].hasOwnProperty('dispatcherPhones'))
      search.phones = orgObj['phone'] + " , " + orgObj['AdditionalData']['dispatcherPhones'];
    else
      search.phones = orgObj['phone'];

    if (search.phones === undefined) search.phones = '';
    search.phones = search.phones.replace(re, '');

    // Extract site urls
    if (orgObj['url'] != undefined)
      search.sites = orgObj['url'];
    else
      search.sites = '';

    search.sites = search.sites.replace(re, '');

    // Emails
    if (orgObj['orgEmail'] != undefined)
      search.emails = orgObj['orgEmail'];
    else
      search.emails = '';

    search.emails = search.emails.replace(re, '');
  }

  if (orgObj.hasOwnProperty('organizationRoles')) {

    if (orgObj['organizationRoles'].length > 0) {
      for (var kk in orgObj['organizationRoles']) {
        if (orgObj['organizationRoles'][kk]['role']['shortName'].length > 8)
          continue;
        var [name, short_name] = [
          orgObj['organizationRoles'][kk]['role']['organizationRoleName'],
          orgObj['organizationRoles'][kk]['role']['shortName']
          ]
      }
    }
    if (short_name !== undefined) {
      var [rt, rf] = await connection.query(
        `SELECT id FROM t4qz8_organization_type WHERE short_name='${short_name}'`
        );
      if (rt.length > 0) {
        search.org_type_id = rt[0].id;
      } else {
        try {
          var [rt1, rf1] = await connection.query(
            `INSERT INTO t4qz8_organization_type (name, short_name) 
            VALUES ('${name}', '${short_name}')`
            );
        } catch(e) {
          logger.debug(name, short_name, e);
        }
        if (rt1 !== undefined && rt1.hasOwnProperty('insertId')) 
          search.org_type_id = rt1.insertId;
      }
    } else {
      // org_type `others`
      search.org_type_id = 7;
    }
  } else {
    // org_type `others`
    search.org_type_id = 7;
  }

  var [rows8, fields8] = await connection.query(
    `SELECT id FROM t4qz8_organization WHERE inn='${orgObj['inn']}';`
    );

  if (rows8.length < 1) {
    try {
      var [rows9, fields9] = await connection.query(
        `INSERT INTO t4qz8_organization (work_time, type_id, house_juridical, name, 
        short_name, inn, ogrn, phones, office_juridical, hits, emails, sites, guid) 
        VALUES ('${search.work_time}', ${search.org_type_id}, '${search.house_id}' , 
        '${orgObj['fullName']}', '${orgObj['shortName']}', '${orgObj['inn']}', 
        '${orgObj['ogrn']}', '${search.phones}', '', 0, '${search.emails}', 
        '${search.sites}', '${orgObj['guid']}')`
        );
      search.org_id = rows9.insertId;
    } catch(e) {
      logger.debug("ORG ERR", e);
    }
  } else {
    search.org_id = rows8[0].id;
    try {
      await connection.query(
        `UPDATE t4qz8_organization SET work_time='${search.work_time}', 
        type_id='${search.org_type_id}', house_juridical='${search.house_id}', 
        guid='${orgObj['guid']}', ogrn='${orgObj['ogrn']}', 
        short_name='${orgObj['shortName']}', name='${orgObj['fullName']}', 
        emails='${search.emails}', sites='${search.sites}', 
        phones='${search.phones}' WHERE id=${search.org_id};`
        );
    } catch(e) {
      logger.debug("ORG ERR", e);
    }
  }

  return search.org_id;
}


/**
  Insert or update org employee
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
  @param orgObj {object} Organization object
*/
exports.InsertEmployee = async (search, conn, logger, orgObj) => {

  var connection = conn;

  if (orgObj['AdditionalData'].hasOwnProperty('chiefInfo')) {
    var fio = orgObj['AdditionalData']['chiefInfo']['fio'];
    var position = orgObj['AdditionalData']['chiefInfo']['position'];
    try {
      var [rows10, fields10] = await connection.query("INSERT INTO t4qz8_organization_employee \
(organization_id, employee_type_id, employee_category_id, first_name, name, patronymic, position) \
VALUES ('"+search.org_id+"', 1, '2', '"+fio+"', '' ,'', '"+position+"') ON DUPLICATE KEY UPDATE \
first_name='"+fio+"', position='"+position+"'");
      search.organization_employee_id = rows10.insertId;
    } catch(e) {
      //pass
    }
    term.saveCursor();
    term.eraseLine();
    term("ORG_ID".green, search.org_id, "ORG EMPLOYEE".yellow, search.organization_employee_id > 0 ? "INSERTED " + search.organization_employee_id : 'UPDATED');
    term.restoreCursor();
  } else {
    term.saveCursor();
    term.eraseLine();
    term("ORG_ID".green, search.org_id, "NO EMPLOYEE".magenta);
    term.restoreCursor();
  }

  return search.organization_employee_id;
}